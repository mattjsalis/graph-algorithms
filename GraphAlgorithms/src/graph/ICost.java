package graph;

/**
 * Interface defining a cost between two nodes
 */
public interface ICost{

    <T> T getCostToChild();
    <T> T getCostToParent();
    <K> K getParentKey();
    <K> K getChildKey();

}
