package graph;

/**
 * Interface for a general graph node
 */
public interface INode {

    /**
     * Determines if the provided node should be considered equivalent.
     * @param node
     * @return
     */
    boolean isEquivalent(INode node);

    /**
     * Determines if this node is properly constrained
     * @return
     */
    boolean isConstrained();

    /**
     * Adds a parent node to this node
     * @param node the parent node
     */
    void addParent(INode node, ICost cost);

    /**
     * Adds a child node to this node
     * @param node the child node
     * @param isBidirectional indicates if the child should maintain a reference to the parent
     */
    void addChild(INode node, ICost cost, boolean isBidirectional);

    <T> T getValue();

    <K> K getKey();
}
