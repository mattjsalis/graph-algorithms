package graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Function;


/**
 * General node class that is identifiable by a key and maintains a value
 * @param <K>
 * @param <V>
 */
public class Node<K, V> implements INode {

    /**
     * List of parent nodes
     */
    private Collection<INode> parentNodes = new ArrayList<>();
    /**
     * List of child nodes
     */
    private Collection<INode> childNodes = new ArrayList<>();
    /**
     * The value associated with this node
     */
    private final V value;

    /**
     * Key associated with this node
     */
    private final K key;

    /**
     * A lambda function that enforces a constraint on this node
     */
    private Function<INode, Boolean> constraintFunction;

    public Node(K key, V value){
        this.key = key;
        this.value = value;
    }

    public Node(K key, V value, Function<INode, Boolean> constraintFunction){
        this(key, value);
        this.constraintFunction = constraintFunction;
    }

    @Override
    public boolean isEquivalent(INode node) {
        return node.getValue().equals(value);
    }

    @Override
    public boolean isConstrained() {
        if(constraintFunction==null){
            return true;
        }
        return constraintFunction.apply(this);
    }

    @Override
    public void addParent(INode node, ICost cost) {
        if(parentNodes.stream().anyMatch(parNode->parNode.equals(node)))
        {
            return;
        }
        parentNodes.add(node);
    }

    @Override
    public void addChild(INode node, ICost cost, boolean isBidirectional) {
        if(childNodes.stream().anyMatch(parNode->parNode.equals(node)))
        {
            return;
        }
        if(isBidirectional){
            node.addParent(this, cost);
        }
        childNodes.add(node);
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }



}
