package graph_test;

import graph.INode;
import graph.Node;
import org.junit.Test;
import java.util.function.Function;

import static org.junit.Assert.*;

public class NodeTest {
    @Test
    public void isEquivalentTest(){
        Node<String, Double> node1 = new Node<>("1", 1.0);
        Node<String, Double> node2 = new Node<>("2", 2.0);
        assert(!node1.isEquivalent(node2));

        node1 = new Node<>("1", 1.0);
        node2 = new Node<>("2", 1.0);
        assert(node1.isEquivalent(node2));
    }

    @Test
    public void isConstrainedTest(){

        Node<String, Double> node1 = new Node<>("1", 1.0);
        assert(node1.isConstrained());

        Function<INode, Boolean> constraint = (node) -> {
            return (Double)node.getValue() > 1.0;
        };

        node1 = new Node<>("1", 1.0, constraint);
        assert(!node1.isConstrained());
        node1 = new Node<>("1", 2.0, constraint);
        assert(node1.isConstrained());


        Function<INode, Boolean> strConstraint = (node) -> {
            return ((String)node.getValue()).contains("world");
        };
        Node<String, String> node2 = new Node<>("1", "Hello, world!", strConstraint);
        assert(node2.isConstrained());
        node2 = new Node<>("1", "Hello, earth!", strConstraint);
        assert(!node2.isConstrained());

    }
}
